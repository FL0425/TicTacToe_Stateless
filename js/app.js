import React from 'react';
import ReactDom from 'react-dom';
import {Instructions} from './Components/Instructions.js';
import {SingleCase} from './Components/SingleCase.js';

class Tictactoe extends React.Component{
  constructor (props){
    super(props);
    this.state ={
      game:['','','','','','','','',''],
      gameOver: false,
      gameWinner: ""
    };
  }

  singleCaseClicked = function(index){
    if (this.state.gameOver){

      alert('GAME OVER');
      return;
    }


    var currentGame = this.state.game;

    switch (currentGame[index]) {
      case '':
          currentGame[index] = 'x';
        break;

      case 'x':
          currentGame[index] = 'o';
        break;

      case 'o':
          currentGame[index] = 'x';
        break;
      default:
    }
        this.setState({game:currentGame});
        this.CheckGameState();
  }

  CheckGameState()
  {
    if (this.state.game[0] != '' &&
        this.state.game[0] == this.state.game[1] &&
        this.state.game[0] == this.state.game[2])
    {
      this.setState({gameWinner : this.state.game[0],
                     gameOver : true});
    }
    else if (this.state.game[3] != '' &&
             this.state.game[3] == this.state.game[4] &&
             this.state.game[3] == this.state.game[5])
    {
      this.setState({gameWinner : this.state.game[3],
                      gameOver : true});
    }
    else if (this.state.game[6] != '' &&
             this.state.game[6] == this.state.game[7]  &&
             this.state.game[6] == this.state.game[8])
    {
      this.setState({gameWinner : this.state.game[6],
                     gameOver : true});
    }
    else if (this.state.game[0] != '' &&
             this.state.game[0] == this.state.game[3] &&
             this.state.game[0] == this.state.game[6])
    {
      this.setState({gameWinner : this.state.game[0],
                     gameOver : true});
    }
    else if (this.state.game[1] != '' &&
             this.state.game[1] == this.state.game[4] &&
             this.state.game[1] == this.state.game[7])
    {
      this.setState({gameWinner : this.state.game[1],
                     gameOver : true});
    }
    else if (this.state.game[2] != '' &&
             this.state.game[2] == this.state.game[5] &&
             this.state.game[2] == this.state.game[8])
    {
      this.setState({gameWinner : this.state.game[2],
                     gameOver : true});
    }
    else if (this.state.game[2] != '' &&
             this.state.game[2] == this.state.game[4] &&
             this.state.game[2] == this.state.game[6])
    {
      this.setState({gameWinner : this.state.game[2],
                     gameOver : true});
    }
    else if (this.state.game[0] != '' &&
             this.state.game[0] == this.state.game[4] &&
             this.state.game[0] == this.state.game[8])
    {
      this.setState({gameWinner : this.state.game[0],
                     gameOver : true});
    }
}



  render() {
    return (
      <div>
        <Instructions gameOver={this.state.gameOver} gameWinner={this.state.gameWinner} />
        <div class="game">
        { this.state.game.map((item, i) =>
          (<SingleCase key={i} index={i} state={item} handleClick={this.singleCaseClicked.bind(this,i)} />)
          )
        }
      </div>
      </div>);
    }
}


ReactDom.render(<Tictactoe />, document.getElementById('body'));
