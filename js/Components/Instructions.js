import React from 'react';
import ReactDom from 'react-dom';

export class Instructions extends React.Component{
  render (){
    return(
        <div>
          Instructions blablablabalbalba
              {this.props.gameOver ?
                  <div className="gameOver">
                    <h1>GAME OVER</h1>
                    <h3> The winner is {this.props.gameWinner} </h3>
                  </div>
              : null}
        </div>)
  }

}

Instructions.propTypes = {
  gameOver : React.PropTypes.bool.isRequired,
  gameWinner : React.PropTypes.string
};
