import React from 'react';
import ReactDom from 'react-dom';
import styles from '../../css/TicTacToe.css';

export class SingleCase extends React.Component{
  render () {
        return (<div className={styles.singleCase}
                    onClick={()=>this.props.handleClick(this.props.index)}>{this.props.state}
                </div>);
  }
}

SingleCase.propTypes = {
  index : React.PropTypes.number.isRequired,
  state : React.PropTypes.oneOf(['', 'x', 'o']).isRequired,
  handleClick : React.PropTypes.func.isRequired
};
