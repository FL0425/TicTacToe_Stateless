var webpack = require('webpack');
var path = require('path');

module.exports = {

  entry: {
    app:['./js/app.js']
  },

  output: {
      path: path.resolve(__dirname, "build"),
      publicPath: "/build/",
      filename: "finalApp.js"

  },

  debug:true,
  devtool: "#eval-source-map",

  module: {
    loaders: [
        {
          test: /\.js?$/,
          loaders:['babel'],
          presets: ['es2016', 'react'],
          exclude: /node_modules/
        },
        {
          test: /\.js$/,
          loader:'babel-loader',
          presets: ['es2016', 'react'],
          exclude: /node_modules/
        },
        {
      	  test: /\.css$/,
      	  loaders: [
        	'style-loader',
        	'css-loader?sourceMap&modules&importLoaders=1&localIdentName=[name]__[local]___[hash:base64:5]'
      	  ]
    	}
    ]
  },

  plugins: [
      new webpack.NoErrorsPlugin()
  ],

  devServer: {
    host: 'localhost',
    port: 8082
  }
};
